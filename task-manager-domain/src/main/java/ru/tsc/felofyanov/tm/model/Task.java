package ru.tsc.felofyanov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.model.IWBS;
import ru.tsc.felofyanov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnerModel implements IWBS {

    private static final long serialVersionUID = 1;

    @Nullable
    private String projectId = null;

    public Task(@NotNull String name, @NotNull Status status, @NotNull Date created) {
        super(name, status, created);
    }
}

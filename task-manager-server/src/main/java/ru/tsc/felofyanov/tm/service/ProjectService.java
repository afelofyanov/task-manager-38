package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.IProjectService;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.repository.ProjectRepository;

import java.sql.Connection;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection, Project.class);
    }
}

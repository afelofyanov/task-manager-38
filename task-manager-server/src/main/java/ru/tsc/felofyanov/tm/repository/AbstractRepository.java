package ru.tsc.felofyanov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IRepository;
import ru.tsc.felofyanov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row);

    @Nullable
    @Override
    public abstract M add(@Nullable final M model);

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final String sql = "SELECT * FROM " + getTableName();
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        @NotNull final List<M> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " ORDER BY ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, "");
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        @NotNull final List<M> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        return result;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = "DELETE FROM " + getTableName();
        @NotNull final Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();
    }

    @NotNull
    @Override
    public Collection<M> add(@Nullable final Collection<M> collection) {
        collection
                .forEach(model -> models.add(add(model)));
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null) return false;
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null) return null;
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE ID =? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final M model = fetch(resultSet);
        statement.close();
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null) return null;
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final M entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final String sql = "DELETE FROM " + getTableName() + " WHERE ID = ?";
        @NotNull PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, model.getId());
        statement.executeUpdate();
        statement.close();
        return model;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null) return;
        collection
                .stream()
                .forEach(this::remove);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        @Nullable final Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        @Nullable final Optional<M> model = Optional.ofNullable(findOneByIndex(index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    @SneakyThrows
    public long count() {
        @NotNull final String sql = "SELECT COUNT(1) FROM " + getTableName();
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        if (!resultSet.next()) return 0;
        return resultSet.getLong(1);
    }
}

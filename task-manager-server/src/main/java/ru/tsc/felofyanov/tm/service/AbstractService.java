package ru.tsc.felofyanov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IRepository;
import ru.tsc.felofyanov.tm.api.repository.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.IService;
import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.IdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.IndexIncorrectException;
import ru.tsc.felofyanov.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @Nullable final M result;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.add(model);
            connection.commit();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.commit();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> add(@NotNull Collection<M> models) {
        @Nullable final Collection<M> result;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.add(models);
            connection.commit();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.commit();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> set(@NotNull Collection<M> models) {
        @NotNull final Connection connection = getConnection();
        @Nullable final Collection<M> result;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M update(@Nullable M model) {
        if (model == null) return null;
        @NotNull final Connection connection = connectionService.getConnection();
        @Nullable M result;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.update(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(comparator);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(sort.getComparator());
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneByIndex(index);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.remove(model);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.removeById(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.removeByIndex(index);
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeAll(collection);
        }
    }

    @Override
    @SneakyThrows
    public long count() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.count();
        }
    }
}

package ru.tsc.felofyanov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.exception.field.DescriptionEmptyException;
import ru.tsc.felofyanov.tm.exception.field.NameEmptyException;
import ru.tsc.felofyanov.tm.exception.field.UserIdEmptyException;
import ru.tsc.felofyanov.tm.model.AbstractUserOwnerModel;

import java.sql.*;
import java.util.*;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel>
        extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @NotNull
    private final Class<M> model;

    public AbstractUserOwnerRepository(@NotNull Connection connection, @NotNull Class<M> model) {
        super(connection);
        this.model = model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final M result = model.newInstance();
        result.setUserId(userId);
        result.setName(name);
        return add(result);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable String userId, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final M result = model.newInstance();
        result.setUserId(userId);
        result.setName(name);
        result.setDescription(description);
        return add(result);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE USER_ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) result.add(fetch(resultSet));
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator) {
        if (userId == null) return Collections.emptyList();
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE USER_ID = ? ORDER BY ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, comparator.toString());
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) result.add(fetch(resultSet));
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                " (ID, NAME, DESCRIPTION, USER_ID, STATUS, CREATED)" +
                " VALUES (?, ?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getName());
            statement.setString(3, model.getDescription());
            statement.setString(4, model.getUserId());
            statement.setString(5, model.getStatus().toString());
            statement.setTimestamp(6, new Timestamp(model.getCreated().getTime()));
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE USER_ID = ? AND ID = ? LIMIT 1";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE USER_ID = ? LIMIT 1 OFFSET ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @Nullable final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    @SneakyThrows
    public long count(@Nullable final String userId) {
        if (userId == null) return 0;
        @NotNull final String sql = "SELECT COUNT(1) FROM " + getTableName() + " WHERE USER_ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        if (!resultSet.next()) return 0;
        return resultSet.getLong(1);
    }
}
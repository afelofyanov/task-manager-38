package ru.tsc.felofyanov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IUserRepository;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_USER";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("ID"));
        user.setLogin(row.getString("LOGIN"));
        user.setPasswordHash(row.getString("PASSWORD"));
        user.setEmail(row.getString("EMAIL"));
        user.setFirstName(row.getString("FST_NAME"));
        user.setFirstName(row.getString("LAST_NAME"));
        user.setFirstName(row.getString("MID_NAME"));
        user.setLocked(row.getBoolean("LOCKED"));
        user.setRole(Role.toRole(row.getString("ROLE")));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User add(@Nullable User user) {
        if (user == null) return null;
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                " (id, login, password, email, fst_name, mid_name, last_name, locked, role)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getEmail());
        statement.setString(5, user.getFirstName());
        statement.setString(6, user.getLastName());
        statement.setString(7, user.getMiddleName());
        statement.setBoolean(8, user.getLocked());
        statement.setString(9, user.getRole().toString());
        statement.executeUpdate();
        statement.close();
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull User user) {
        @NotNull final String sql = "UPDATE " + getTableName() +
                " SET LOGIN = ?, PASSWORD = ?, EMAIL = ?, FST_NAME = ?, LAST_NAME = ?, MID_NAME = ?, LOCKED = ?," +
                " ROLE = ? WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPasswordHash());
        statement.setString(3, user.getEmail());
        statement.setString(4, user.getFirstName());
        statement.setString(5, user.getLastName());
        statement.setString(6, user.getMiddleName());
        statement.setBoolean(7, user.getLocked());
        statement.setString(8, user.getRole().toString());
        statement.setString(9, user.getId());
        statement.executeUpdate();
        statement.close();
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null) return null;
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE LOGIN = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null) return null;
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE EMAIL = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null) return null;
        @Nullable final Optional<User> model = Optional.ofNullable(findByLogin(login));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }
}

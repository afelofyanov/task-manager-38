package ru.tsc.felofyanov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);
}

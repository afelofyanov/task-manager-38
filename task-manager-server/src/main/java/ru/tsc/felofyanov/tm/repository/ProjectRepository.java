package ru.tsc.felofyanov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_PROJECT";

    public ProjectRepository(@NotNull Connection connection, @NotNull Class<Project> model) {
        super(connection, model);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Project fetch(@NotNull final ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString("ID"));
        project.setName(row.getString("NAME"));
        project.setDescription(row.getString("DESCRIPTION"));
        project.setUserId(row.getString("USER_ID"));
        project.setStatus(Status.toStatus(row.getString("STATUS")));
        project.setCreated(row.getTimestamp("CREATED"));
        project.setDateBegin(row.getTimestamp("START_DT"));
        project.setDateEnd(row.getTimestamp("END_DT"));
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public final Project add(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                " (ID, NAME, DESCRIPTION, USER_ID, STATUS, CREATED)" +
                " VALUES (?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getUserId());
        statement.setString(5, project.getStatus().toString());
        statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
        statement.executeUpdate();
        statement.close();
        return project;
    }

    @Nullable
    @Override
    public Project add(@Nullable String userId, @Nullable Project project) {
        if (project == null) return null;
        if (userId == null || userId.isEmpty()) return null;
        project.setUserId(userId);
        @Nullable final Project result = add(project);
        return result;
    }

    @NotNull
    @SneakyThrows
    public final Project update(@NotNull final Project project) {
        @NotNull final String sql = "UPDATE " + getTableName() +
                " SET NAME = ?, DESCRIPTION = ?, USER_ID = ?, STATUS = ? WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, project.getName());
        statement.setString(2, project.getDescription());
        statement.setString(3, project.getUserId());
        statement.setString(4, project.getStatus().toString());
        statement.setString(5, project.getId());
        statement.executeUpdate();
        statement.close();
        return project;
    }
}
